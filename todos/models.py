from django.db import models

# Create your models here.

#creating a Todolist Model that will allow us to make a list
class TodoList(models.Model):
    #have to give todo list a name
    name = models.CharField(max_length = 100)
    #When was the list created
    created_on = models.DateTimeField(auto_now_add=True)
    # a function that will return the name of the list
    def __str__(self):
        return self.name 

#create a todoItem Model that will allow me populate my todolsit
class TodoItem(models.Model):
    #name a task
    task = models.CharField(max_length=100)
    #duedate property, it is optional! 
    due_date =models.DateTimeField(null=True, blank=True )
    #is complete property, a boolean??
    is_completed =models.BooleanField(default=False)
    # a foreign key relationship, cascade.delete 
    list= models.ForeignKey("TodoList", related_name="items", on_delete=models.CASCADE)
    # a function that returns the value of the items 
    def __str__(self):
        return self.task 

