from django.urls import path 

#from todos.views import TodoListList

#urlpatterns =[
  #  path("", TodoListView.as_view(), name = "todo_list")
#]

from todos.views import (TodoListListView,  TodoListDetailView, TodoListCreateView)

    

urlpatterns = [
    #make path, name of class, url path
    path("", TodoListListView.as_view(), name="todo_list_list"),
    path("<int:pk>/", TodoListDetailView.as_view(), name="todo_list_detail"),
    path("create/", TodoListCreateView.as_view(), name="todo_list_create")]